class CreateTrips < ActiveRecord::Migration[5.1]
  def change
    create_table :trips do |t|
      t.references :driver
      t.references :source
      t.references :destination
      t.datetime :departure_time
      t.integer :seats

      t.timestamps
    end
  end
end
