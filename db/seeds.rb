# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

groups = Group.create([{ name: 'Axis' }, { name: 'Allies' }])
places = Place.create([{ name: 'Maadi', longitude: 32.91663, latitude: 96.982841 }, { name: 'Zamalek', longitude: 40.91663, latitude: 100.982841 }, { name: 'Smart Village', longitude: 50.91663, latitude: 95.982841 }, { name: 'New Cairo', longitude: 55.91663, latitude: 105.982841 }])


