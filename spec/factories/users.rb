FactoryGirl.define do
  factory :user do
    first_name { Faker::Lorem.word }
    last_name { Faker::Lorem.word }
    phone_number { Faker::PhoneNumber.phone_number }
    group { FactoryGirl.create(:group) }
    association :home_place, factory: :place
    association :work_place, factory: :place
  end
end