FactoryGirl.define do
  factory :trip do
    association :driver, factory: :user 
    association :source, factory: :place
    association :destination, factory: :place
    departure_time { Faker::Time.between(5.days.ago, Date.today, :all) }
    seats { Faker::Number.between(1, 10) }
  end
end