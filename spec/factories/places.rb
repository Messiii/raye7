FactoryGirl.define do
  factory :place do
    name { Faker::Lorem.word }
    longitude { Faker::Number.decimal(10) }
    latitude { Faker::Number.decimal(10) }
    # association :user_home, factory: :user
    # association :user_work, factory: :user
  end
end
