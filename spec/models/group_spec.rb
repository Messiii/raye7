require 'rails_helper'

RSpec.describe Group, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"

    it { should have_many(:users)}

    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }

end
