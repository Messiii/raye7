require 'rails_helper'

RSpec.describe User, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
    it { should belong_to(:group) }
    it { should belong_to(:home_place) }
    it { should belong_to(:work_place) }
    it { should have_many(:driver_trips)}
    it { should have_many(:user_trips)}
    it { should have_many(:trips)}
    
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:phone_number) }
    it { should validate_presence_of(:group_id) }
    it { should validate_presence_of(:home_place_id) }
    it { should validate_presence_of(:work_place_id) }
end
