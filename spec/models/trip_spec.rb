require 'rails_helper'

RSpec.describe Trip, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
    it { should belong_to(:driver) }
    it { should belong_to(:source) }
    it { should belong_to(:destination) }
    it { should have_many(:user_trips) }
    it { should have_many(:users) }

    
    it { should validate_presence_of(:driver_id) }
    it { should validate_presence_of(:source_id) }
    it { should validate_presence_of(:destination_id) }
    it { should validate_presence_of(:departure_time) }
    it { should validate_presence_of(:seats) }
    
end
