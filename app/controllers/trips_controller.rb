class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :update, :destroy]

  # GET /trips
  def index
    @user = User.find(params[:user_id])
    @trips =  Trip.joins(:driver).where(:users => { :group_id => @user.group_id })
    @trips.trips_lambda(@user,@trips)
    render json: @trips
  end

  # GET /trips/1
  def show
    @users = @trip.users
    render json: @trip.to_json(:include => [:users])
  end

  # POST /trips
  def create
    @trip = Trip.new(trip_params)

    if @trip.save
      render json: @trip, status: :created, location: @trip
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /trips/1
  def update
    if @trip.update(trip_params)
      render json: @trip
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  # DELETE /trips/1
  def destroy
    @user = User.find(params["user_id"])
    @user.driver_trips.destroy_all
  end

  def join
    @trip = Trip.find(params["trip_id"])
    user = User.find(params["user_id"])
    @trip.users << user
    render json: @trip.to_json(:include => [:users])
  end
  
  def leave
    trip = Trip.find(params["trip_id"])
    if trip.user_trips.where(user_id: params["user_id"]).destroy_all
      render :json => trip, status: 200
    else
      render :json => trip, :status => :bad_request
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip
      @trip = Trip.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def trip_params
      params.require(:trip).permit(:driver_id, :source_id, :destination_id, :departure_time, :seats)
    end
end
