class Trip < ApplicationRecord
  belongs_to :driver, :class_name => 'User'
  belongs_to :source, :class_name => 'Place'
  belongs_to :destination, :class_name => 'Place'
  has_many :user_trips
  has_many :users, :through => :user_trips
  validates :driver_id, :source_id, :destination_id, :departure_time, :seats, presence: true
  
   scope :trips_lambda, -> (user , trip) { where('source_id=? OR destination_id=?', user.home_place_id, user.work_place_id).or(trip.where('source_id=? OR destination_id=?', user.work_place_id, user.home_place_id)) }
end
