class User < ApplicationRecord
    belongs_to :group
    belongs_to :home_place, :class_name => 'Place', :foreign_key => 'home_place_id'
    belongs_to :work_place, :class_name => 'Place', :foreign_key => 'work_place_id'
    
    has_many :driver_trips , :foreign_key => "driver_id", :class_name => "Trip"
    has_many :user_trips
    has_many :trips, :through => :user_trips  
    
    validates :first_name,:last_name,:phone_number, :group_id, :home_place_id, :work_place_id,  presence: true
end
