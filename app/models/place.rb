class Place < ApplicationRecord
  has_many :user_home, :class_name => 'User', :foreign_key => 'home_place_id'
  has_many :user_work, :class_name => 'User', :foreign_key => 'work_place_id'
  
  validates :name, presence: true, uniqueness: true
end
