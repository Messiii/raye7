Rails.application.routes.draw do

  resources :places
  resources :groups
  resources :users
  resources :trips do
    post "/join" => "trips#join", :as => :join_trip
    post "/leave" => "trips#leave", :as => :leave_trip
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
